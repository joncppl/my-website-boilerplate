// @flow
import React, { Component } from 'react';
import { Grid, Col, Row } from 'react-bootstrap';

export default class HomePage extends Component {
  render() {
    return (
      <Grid>
        <Row>
          <Col xs={6}>
            <h1> Left </h1>
          </Col>
          <Col xs={6}>
            <h1> Right </h1>
          </Col>
        </Row>
      </Grid>
    );
  }
}

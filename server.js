/* eslint-disable no-console */
/**
 * Setup and run the development server for Hot-Module-Replacement
 * https://webpack.github.io/docs/hot-module-replacement-with-webpack.html
 * @flow
 */

import fs from 'fs';
import express from 'express';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import portfinder from 'portfinder';

import config from './webpack.config.development';

const app = express();
const compiler = webpack(config);
const PORT = process.env.PORT || 3000;

const wdm = webpackDevMiddleware(compiler, {
  publicPath: config.output.publicPath,
  stats: {
    colors: true
  }
});

app.use(wdm);

app.use(webpackHotMiddleware(compiler));

portfinder.basePort = PORT;
portfinder.getPort((porterr, port) => {
  if (porterr) {
    console.error(porterr);
    process.exit(1);
  }

  const title = 'My App';

  const ret = `<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <title>${title}</title>
    </head>
    <body>
      <div id="root"></div>
      <script>
        {
          const script = document.createElement('script');
          script.src = "http://localhost:${port}/dist/bundle.js";
          document.write(script.outerHTML);
        }
      </script>
    </body>
  </html>
`;

  app.get('/', (req, res) => {
    res.send(ret);
  });

  const server = app.listen(port, 'localhost', err => {
    if (err) {
      console.error(err);
      return;
    }

    console.log(`Listening at http://localhost:${port}`);
  });

  process.on('SIGTERM', () => {
    console.log('Stopping dev server');
    wdm.close();
    server.close(() => {
      process.exit(0);
    });
  });
});
